import xml.etree.ElementTree as ET  # for parsing the xml
from PIL import Image, ImageDraw    # for drawing rectangles
import sys                          # for reading command line args
import os                           # for displaying the image after it's generated

class Parser(object):

    def __init__(self):
        self.xml = None
        self.png = None
        self.img = None
        self.drawer = None
        self.cmd = None
        self.tree = None
        self.root = None
        self.found_files = []
        self.path = None

    def __open_files(self):
        self.tree = ET.parse(self.path + self.xml)
        self.root = self.tree.getroot()

        self.img = Image.open(self.path + self.png)
        self.drawer = ImageDraw.Draw(self.img)

        self.__find_leaves()


    def __find_leaves(self):
        for node in self.root.iter('node'):
            # IF NODE DOES NOT HAVE CHILDREN...
            if not node: # len(list(node))
                self.__extract_coords(node)
        self.__save_img()



    def __extract_coords(self, node):
        mystr = node.get('bounds').replace('[', ' ', 10)
        mystr = mystr.replace(']', ' ', 10)
        mystr = mystr.replace(',', ' ', 10)
        mylist = [int(x) for x in mystr.split()]
        self.drawer.rectangle(tuple(mylist), outline=(255,255,0), width=5)

    def __save_img(self):
        self.img.save("marked." + self.png)
        print("A marked image was saved as:", "marked." + self.png)
        self.cmd = "eog " + "marked." + self.png
        # self.__display_img()

    def display_img(self):
        open_img = input("Would you like to view an image that was generated (using oeg)? Enter y/n: ")
        if open_img is 'Y' or open_img is 'y':
            os.system(self.cmd)
        else:
            sys.exit("\nThe image files, along with the broken xml, are now modified.")

    def __fix_xml(self):
        lines_of_file = None
        with open(self.path + self.xml, 'r') as my_file:
            lines_of_file = my_file.readlines()
            lines_of_file.insert(-1, "</node>")
        with open(self.path + self.xml, 'w') as my_file:
            my_file.writelines(lines_of_file)

    def find_target_files(self):
        if len(sys.argv) > 2:
            print("Too many arguments were given. Only an absolute file path should be given.")
            return
        elif len(sys.argv) < 2:
            print("A file path must be given.")
            return

        self.path = sys.argv[1]
        if not self.path.endswith("/"):
            self.path = self.path + "/"
        folder = os.fsencode(sys.argv[1])
    
        for file in os.listdir(folder):
            filename = os.fsdecode(file)
            if (filename.endswith(".xml") or filename.endswith(".png")): 
                self.found_files.append(filename)


        for x in self.found_files:
            basepng = x[0:-4] + ".png"
            basexml = x[0:-4] + ".xml"
            if basepng in self.found_files and basexml in self.found_files:
                self.xml = basexml
                self.png = basepng
                try:
                    self.__open_files()
                except ET.ParseError:
                    print("ParseError: file was missing a </node> tag.")
                    self.__fix_xml()
                    self.__open_files()

xml_parser = Parser()
xml_parser.find_target_files()
xml_parser.display_img()

